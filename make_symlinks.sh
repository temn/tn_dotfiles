#!/bin/bash
# This script creates symlinks from the home directory to any desired dotfiles in ~/dotfiles

# Found at
# http://blog.smalleycreative.com/tutorials/using-git-and-github-to-manage-your-dotfiles/

#Help function to see if a command exists
command_exists () {
  type "$1" >/dev/null 2>/dev/null
}

#ORANGE='\033[0;33m'
LBLUE='\033[1;34m'
NC='\033[0m'

########## Variables
dir='/Users/tn/Documents/dotfiles_tn_git'                    # dotfiles directory
olddir='~/.dotfiles_old'             # old dotfiles backup directory
# List of files/folders to symlink in homedir
files="bashrc bash_profile zshrc dircolors"

##########

# create dotfiles_old in homedir
echo -e "${LBLUE}Creating $olddir for backup of any existing dotfiles in ~${NC}"
mkdir -p $olddir
echo "...done"

# change to the dotfiles directory
echo -e "${LBLUE}Changing to the $dir directory${NC}"
cd $dir
echo "...done"

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks
for file in $files; do
    echo "Moving any existing dotfiles from ~ to $olddir"
    mv ~/.$file $olddir
    echo "Creating symlink to $file in home directory."
    ln -s $dir/$file ~/.$file
done

echo -e "${LBLUE}Copying sunrise-modified-ran.zsh-theme file to ~/.oh-my-zsh/themes/${NC}"
cp $dir/oh-my-zsh/themes/sunrise-modified-ran.zsh-theme ~/.oh-my-zsh/themes/
cp -r $dir/oh-my-zsh/plugins/git ~/.oh-my-zsh/plugins/

echo -e "${LBLUE}Reloading bash${NC}"
source ~/.bashrc
echo "...done"

echo -e "${LBLUE}Creating symlink to ~/.ssh/config (ln -s $dir/ssh_config ~/.ssh/config)${NC}"
ln -s $dir/ssh_config ~/.ssh/config
echo "...done"
